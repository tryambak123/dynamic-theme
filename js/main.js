$(document).ready(function(){
	$('.accordion-kvs').accordion({
		collapsible: true,
		active: false
	});
	
	$('#dd-themes').on('change', function(){
		var theme = $(this).val();
		//theme_file = theme + '.php';
		$('#theme-loader').attr('src', theme);
		populateForm(theme);
	});
		
	function populateForm(theme_name){
		$.ajax({
			url: "ajax.php", 
			data : {theme:theme_name},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				//console.log(obj);
				$('#tab_bg_color').val(obj.tab_bg_color);
				$('#tab_text_color').val(obj.tab_text_color);
				$('#accordion_text_color').val(obj.accordion_text_color);
				$('#heading_color').val(obj.heading_color);
				$('#menu_text_color').val(obj.menu_text_color);
				$('#menu_text_hover_color').val(obj.menu_text_hover_color);
				$('#menu_bg_color').val(obj.menu_bg_color);
				$('#menu_hover_bg_color').val(obj.menu_hover_bg_color);
				$('#header_bg_color').val(obj.header_bg_color);
				$('#footer_bottom_bg_color').val(obj.footer_bottom_bg_color);
				$('#footer_bg_color').val(obj.footer_bg_color);
				$('#footer_text_color').val(obj.footer_text_color);
				$('#footer_bottom_text_color').val(obj.footer_bottom_text_color);
				$('#footer_heading_color').val(obj.footer_heading_color);
				$('#menu_nav_bg_color').val(obj.menu_nav_bg_color);
				$('#menu_nav_bg_color_hover').val(obj.menu_nav_bg_color_hover);
				$('#menu_nav_text_color').val(obj.menu_nav_text_color);
				$('#menu_nav_text_color_hover').val(obj.menu_nav_text_color_hover);
				$('#menu_nav_border_color').val(obj.menu_nav_border_color);
				$('#top_header_text_color').val(obj.top_header_text_color);
				$('#top_header_text_color_hover').val(obj.top_header_text_color_hover);
				$('#top_header_bg_color').val(obj.top_header_bg_color);
				$('#product_price_color').val(obj.product_price_color);
				$('#product_price_color_hover').val(obj.product_price_color_hover);
				$('#product_subtitle_color').val(obj.product_subtitle_color);
				$('#product_heading_color').val(obj.product_heading_color);
				$('#product_heading_bgcolor').val(obj.product_heading_bgcolor);
				$('#product_subtitle_color_hover').val(obj.product_subtitle_color_hover);
				$('#validation_fa_icon_paragraph').val(obj.validation_fa_icon_paragraph);
				$('#fa_icon_bgcolor').val(obj.fa_icon_bgcolor);
				$('#fa_icon_bgcolor_hover').val(obj.fa_icon_bgcolor_hover);
				$('#fa_icon_text_color').val(obj.fa_icon_text_color);
				$('#fa_icon_text_color_hover').val(obj.fa_icon_text_color_hover);
				$('#paragraph_color').val(obj.paragraph_color);
				$('#validation_color').val(obj.validation_color);
			}
		});
	}
	
	$('.applyChanges').on('click', function(){
		section = $(this).attr('section');
		frm_data = $('#frm-'+section).serializeArray();
		//console.log(frm_data);
		
		var arr = {};
		$.each(frm_data, function(key, data){
			var res = data.name.split("_p_");
			console.log('aaa ' + res[1]);
			if(typeof res[1] != 'undefined' && res[1] != 'undefined'){
				master_class = res[1].replace(/=dot=/g,'.');
				master_class = master_class.replace(/=sp=/g,' ');
			} else {
				//console.log('master_class = ' + master_class);
			}
			
			style_name = res[0];
			
			if(typeof(arr[master_class]) != "undefined" && arr[master_class] !== null){
				if(typeof(arr[master_class][style_name]) != "undefined" && arr[master_class][style_name] !== null){
					arr[master_class][style_name] = data.value;
				} else {
					arr[master_class][style_name] = data.value;
				}
			} else {
				arr[master_class] = {};
				arr[master_class][style_name] = data.value;
			}
			
		});
		
		//console.log(arr);
		$.each(arr, function(class_name, styles){
			console.log('class_name = '+class_name);
			$.each(styles, function(key, value){
				if(class_name.indexOf('.footer .') != -1){
					class_name = class_name.replace('.footer','',class_name);
					$("#theme-loader").contents().find(class_name).css(key, value);
				} else if(class_name.indexOf('.footer') != -1){
					$("#theme-loader").contents().find(class_name).css(key, value);
				} else {
					$("#theme-loader").contents().find(".page-wrapper " + class_name).css(key, value);
				}
			});
			
		});
		return false;
	});
				
});