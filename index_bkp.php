<html>
<?php 
include('functions.php');
$file = 'css/theme1.css';

if(isset($_GET['msg'])){ 
	if($_GET['msg'] == 'success'){ ?>
		<p style="color:green">Changes saved successfully</p><?php
	} else { ?>
		<p style="color:red">Error in saving config changes</p><?php
	}
}

$theme_id = 1;
$theme_data = getThemeData($theme_id);
$themes = getThemeList(); ?>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<!-- Optional theme -->
		<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="css/custom.css">
	</head>
	<body>
		<div class="container" style="border:1px solid #cdcdcd;">
			<div class="row">
				<div class="col-lg-4 col">
					<form name="frm-theme-config" id="frm-theme-config" method="post" action="saveChanges.php">
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
										<div class="caption">Themes</div>
										<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-1">
										<div class="row">
											<div class="col-md-6">
												<select name="dd-themes" id="dd-themes"><?php 
													foreach($themes as $key => $data){?>
														<option value="<?php echo $data['name'] ?>"><?php echo $data['name']?></option><?php
													}?>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
								<div class="col-lg-12 col">
									<div class="portlet box portlet-grey">
										<div class="portlet-header">
											<div class="caption">General Color Setting</div>
											<div class="tools"></div>
										</div>
										<div class="portlet-body portlet-body-row-1">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="text_color" class="control-label">Text Color <span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="text_color" name="text_color" value="<?php echo $theme_data['text_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="site_bg_color" class="control-label">Site BG Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="site_bg_color" name="site_bg_color" value="<?php echo $theme_data['site_bg_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="button_bg_color" class="control-label">Button BG Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="button_bg_color" name="button_bg_color" value="<?php echo $theme_data['button_bg_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="button_text_color" class="control-label">Button Text Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="button_text_color" name="button_text_color" value="<?php echo $theme_data['button_text_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="button_hover_color" class="control-label">Button Hover Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="button_hover_color" name="button_hover_color" value="<?php echo $theme_data['button_hover_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="button_hover_text_color" class="control-label">Button Hover Text Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="button_hover_text_color" name="button_hover_text_color" value="<?php echo $theme_data['button_hover_text_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="tab_bg_color" class="control-label">Tab BG Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="tab_bg_color" name="tab_bg_color" value="<?php echo $theme_data['tab_bg_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="tab_text_color" class="control-label">Tab Text Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="tab_text_color" name="tab_text_color" value="<?php echo $theme_data['tab_text_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
													</div>
												</div>
											</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="accordion_bg_color" class="control-label">Accordion BG Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="accordion_bg_color" name="accordion_bg_color" value="<?php echo $theme_data['accordion_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="accordion_text_color" class="control-label">Accordion Text Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="accordion_text_color" name="accordion_text_color" value="<?php echo $theme_data['accordion_text_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="heading_color" class="control-label">Heading Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="heading_color" name="heading_color" value="<?php echo $theme_data['heading_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										</div>
									</div>
								</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
									<div class="caption">Menu &amp; Header Color Setting</div>
									<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-1">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_text_color" class="control-label">Menu Text Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_text_color" name="menu_text_color" value="<?php echo $theme_data['menu_text_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_text_hover_color" class="control-label">Menu Text Hover Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_text_hover_color" name="menu_text_hover_color" value="<?php echo $theme_data['menu_text_hover_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_bg_color" class="control-label">Menu BG Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_bg_color" name="menu_bg_color" value="<?php echo $theme_data['menu_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_hover_bg_color" class="control-label">Menu Hover BG Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_hover_bg_color" name="menu_hover_bg_color" value="<?php echo $theme_data['menu_hover_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
										<div class="caption">Header, Footer &amp; Logo</div>
										<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-1">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="header_bg_color" class="control-label">Header BG Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="header_bg_color" name="header_bg_color" value="<?php echo $theme_data['header_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="footer_bottom_bg_color" class="control-label">Footer Bottom BgColor<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="footer_bottom_bg_color" name="footer_bottom_bg_color" value="<?php echo $theme_data['footer_bottom_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="footer_bg_color" class="control-label">Footer BG Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="footer_bg_color" name="footer_bg_color" value="<?php echo $theme_data['footer_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="footer_text_color" class="control-label">Footer Text Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="footer_text_color" name="footer_text_color" value="<?php echo $theme_data['footer_text_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="footer_bottom_text_color" class="control-label">Footer Bottom Text Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="footer_bottom_text_color" name="footer_bottom_text_color" value="<?php echo $theme_data['footer_bottom_text_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="footer_heading_color" class="control-label">Footer Heading Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="footer_heading_color" name="footer_heading_color" value="<?php echo $theme_data['footer_heading_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
										<div class="caption">Sub Category Color Setting</div>
										<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-2">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_nav_bg_color" class="control-label">Menu Nav Bg Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_nav_bg_color" name="menu_nav_bg_color" value="<?php echo $theme_data['menu_nav_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_nav_bg_color_hover" class="control-label">Nav Bg Color Hover<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_nav_bg_color_hover" name="menu_nav_bg_color_hover" value="<?php echo $theme_data['menu_nav_bg_color_hover']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_nav_text_color" class="control-label">Menu Nav Text Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_nav_text_color" name="menu_nav_text_color" value="<?php echo $theme_data['menu_nav_text_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_nav_text_color_hover" class="control-label">Nav Text Color Hover<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_nav_text_color_hover" name="menu_nav_text_color_hover" value="<?php echo $theme_data['menu_nav_text_color_hover']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="menu_nav_border_color" class="control-label">Menu Nav Border Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="menu_nav_border_color" name="menu_nav_border_color" value="<?php echo $theme_data['menu_nav_border_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
										<div class="caption">Top Header and Product Setting</div>
										<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-2">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="top_header_text_color" class="control-label">Top Header Text Color<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="top_header_text_color" name="top_header_text_color" value="<?php echo $theme_data['top_header_text_color']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="top_header_text_color_hover" class="control-label">Top Text Color Hover<span class="require">*</span></label>
														<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
															<input type="color" id="top_header_text_color_hover" name="top_header_text_color_hover" value="<?php echo $theme_data['top_header_text_color_hover']?>" class="form-control required">
														</div>
														<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="top_header_bg_color" class="control-label">Top Header Bg Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="top_header_bg_color" name="top_header_bg_color" value="<?php echo $theme_data['top_header_bg_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="product_price_color" class="control-label">Product Price Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_price_color" name="product_price_color" value="<?php echo $theme_data['product_price_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="product_price_color_hover" class="control-label">Prod. Price-Col-Hover <span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_price_color_hover" name="product_price_color_hover" value="<?php echo $theme_data['product_price_color_hover']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="product_subtitle_color" class="control-label">Prod Subtitle Color <span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_subtitle_color" name="product_subtitle_color" value="<?php echo $theme_data['product_subtitle_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="product_heading_color" class="control-label">Prod Heading Color<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_heading_color" name="product_heading_color" value="<?php echo $theme_data['product_heading_color']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="product_heading_bgcolor" class="control-label">Prod Heading Bgcolor<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_heading_bgcolor" name="product_heading_bgcolor" value="<?php echo $theme_data['product_heading_bgcolor']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="product_subtitle_color_hover" class="control-label">Product Sub title Color Hover<span class="require">*</span></label>
													<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
														<input type="color" id="product_subtitle_color_hover" name="product_subtitle_color_hover" value="<?php echo $theme_data['product_subtitle_color_hover']?>" class="form-control required">
													</div>
													<i class="alert alert-hide"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col">
								<div class="portlet box portlet-grey">
									<div class="portlet-header">
									<div class="caption">FA Icon and Validation Setting</div>
									<div class="tools"></div>
									</div>
									<div class="portlet-body portlet-body-row-2">
									<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="validation_fa_icon_paragraph" class="control-label">Validation Fa Icon Paragraph<span class="require">*</span></label>
											<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
												<input type="color" id="validation_fa_icon_paragraph" name="validation_fa_icon_paragraph" value="<?php echo $theme_data['validation_fa_icon_paragraph']?>" class="form-control required">
											</div>
											<i class="alert alert-hide"></i>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="fa_icon_bgcolor" class="control-label">Fa Icon Bg Color<span class="require">*</span></label>
											<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
												<input type="color" id="fa_icon_bgcolor" name="fa_icon_bgcolor" value="<?php echo $theme_data['fa_icon_bgcolor']?>" class="form-control required">
											</div>
											<i class="alert alert-hide"></i>
										</div>
									</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="fa_icon_bgcolor_hover" class="control-label">Fa Icon Bg Color Hover<span class="require">*</span></label>
												<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
													<input type="color" id="fa_icon_bgcolor_hover" name="fa_icon_bgcolor_hover" value="<?php echo $theme_data['fa_icon_bgcolor_hover']?>" class="form-control required">
												</div>
												<i class="alert alert-hide"></i>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="fa_icon_text_color" class="control-label">Fa Icon Text Color<span class="require">*</span></label>
												<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
													<input type="color" id="fa_icon_text_color" name="fa_icon_text_color" value="<?php echo $theme_data['fa_icon_text_color']?>" class="form-control required">
												</div>
												<i class="alert alert-hide"></i>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="fa_icon_text_color_hover" class="control-label">Fa Icon Text Color Hover</label>
												<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
													<input type="color" id="fa_icon_text_color_hover" name="fa_icon_text_color_hover" value="<?php echo $theme_data['fa_icon_text_color_hover']?>" class="form-control required">
												</div>
												<i class="alert alert-hide"></i>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="paragraph_color" class="control-label">Paragraph Color</label>
												<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
													<input type="color" id="paragraph_color" name="paragraph_color" value="<?php echo $theme_data['paragraph_color']?>" class="form-control required">
												</div>
												<i class="alert alert-hide"></i>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
										<div class="form-group">
										<label for="validation_color" class="control-label">Validation Color</label>
												<div data-color="#333" data-color-format="rgba" class="input-group colorpicker-component-1">
													<input type="color" id="validation_color" name="validation_color" value="<?php echo $theme_data['validation_color']?>" class="form-control required">
												</div>
												<i class="alert alert-hide"></i></div>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col">
								<button value="Apply" type="button" id="applyChanges" name="applyChanges" class="btn btn-secondary">Apply</button>
							</div>
							<div class="col-lg-6 col">
								<button value="Save" type="submit" name="submit" id="saveChanges" class="btn btn-primary">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-8 col" style="background:#cdcdcd">
					<iframe id="theme-loader" src="theme1" style="width:100%;height:100%;"></iframe>
				</div>
			</div>
		</div><?php 
		if(isset($_POST['submit'])){
			echo '<pre>';print_r($_POST);die;
		}?>
	</body>
<script>
	$(document).ready(function(){
		$('#dd-themes').on('change', function(){
			var theme = $(this).val();
			//theme_file = theme + '.php';
			$('#theme-loader').attr('src', theme);
			populateForm(theme);
		});
		
		function populateForm(theme_name){
			$.ajax({
				url: "ajax.php", 
				data : {theme:theme_name},
				success: function(result){
					var obj = jQuery.parseJSON(result);
					//console.log(obj);
					$('#tab_bg_color').val(obj.tab_bg_color);
					$('#tab_text_color').val(obj.tab_text_color);
					$('#accordion_text_color').val(obj.accordion_text_color);
					$('#heading_color').val(obj.heading_color);
					$('#menu_text_color').val(obj.menu_text_color);
					$('#menu_text_hover_color').val(obj.menu_text_hover_color);
					$('#menu_bg_color').val(obj.menu_bg_color);
					$('#menu_hover_bg_color').val(obj.menu_hover_bg_color);
					$('#header_bg_color').val(obj.header_bg_color);
					$('#footer_bottom_bg_color').val(obj.footer_bottom_bg_color);
					$('#footer_bg_color').val(obj.footer_bg_color);
					$('#footer_text_color').val(obj.footer_text_color);
					$('#footer_bottom_text_color').val(obj.footer_bottom_text_color);
					$('#footer_heading_color').val(obj.footer_heading_color);
					$('#menu_nav_bg_color').val(obj.menu_nav_bg_color);
					$('#menu_nav_bg_color_hover').val(obj.menu_nav_bg_color_hover);
					$('#menu_nav_text_color').val(obj.menu_nav_text_color);
					$('#menu_nav_text_color_hover').val(obj.menu_nav_text_color_hover);
					$('#menu_nav_border_color').val(obj.menu_nav_border_color);
					$('#top_header_text_color').val(obj.top_header_text_color);
					$('#top_header_text_color_hover').val(obj.top_header_text_color_hover);
					$('#top_header_bg_color').val(obj.top_header_bg_color);
					$('#product_price_color').val(obj.product_price_color);
					$('#product_price_color_hover').val(obj.product_price_color_hover);
					$('#product_subtitle_color').val(obj.product_subtitle_color);
					$('#product_heading_color').val(obj.product_heading_color);
					$('#product_heading_bgcolor').val(obj.product_heading_bgcolor);
					$('#product_subtitle_color_hover').val(obj.product_subtitle_color_hover);
					$('#validation_fa_icon_paragraph').val(obj.validation_fa_icon_paragraph);
					$('#fa_icon_bgcolor').val(obj.fa_icon_bgcolor);
					$('#fa_icon_bgcolor_hover').val(obj.fa_icon_bgcolor_hover);
					$('#fa_icon_text_color').val(obj.fa_icon_text_color);
					$('#fa_icon_text_color_hover').val(obj.fa_icon_text_color_hover);
					$('#paragraph_color').val(obj.paragraph_color);
					$('#validation_color').val(obj.validation_color);
				}
			});
		}
		
		$('#applyChanges').on('click', function(){
			button_bg_color = $('#button_bg_color').val();
			$("#theme-loader").contents().find(".btn").css("background-color", button_bg_color);
			text_color = $('#text_color').val();
			$("#theme-loader").contents().find(".container").css("color", text_color);
			site_bg_color = $('#site_background_color').val();
			$("#theme-loader").contents().find(".container").css("background-color", site_bg_color);
			button_hover_color = $('#button_hover_color').val();
			$("#theme-loader").contents().find(".btn").hover(function(){
				$("#theme-loader").contents().find(".btn").css("background-color",button_hover_color);
			});
			
			button_hover_text_color = $('#button_hover_text_color').val();
			$("#theme-loader").contents().find(".btn").hover(function(){
				$("#theme-loader").contents().find(".btn").css("color",button_hover_text_color);
			});
			tab_bg_color = $('#tab_bg_color').val();
			tab_text_color = $('#').val();
			accordion_bg_color = $('#tab_text_color').val();
			accordion_text_color = $('#accordion_text_color').val();
			heading_color = $('#heading_color').val();
			menu_text_color = $('#menu_text_color').val();
			menu_text_hover_color = $('#menu_text_hover_color').val();
			menu_bg_color = $('#menu_bg_color').val();
			menu_hover_bg_color = $('#menu_hover_bg_color').val();
			header_bg_color = $('#header_bg_color').val();
			footer_bottom_bg_color = $('#footer_bottom_bg_color').val();
			footer_bg_color = $('#footer_bg_color').val();
			footer_text_color = $('#footer_text_color').val();
			footer_bottom_text_color = $('#footer_bottom_text_color').val();
			footer_heading_color = $('#footer_heading_color').val();
			menu_nav_bg_color = $('#menu_nav_bg_color').val();
			menu_nav_bg_color_hover = $('#menu_nav_bg_color_hover').val();
			menu_nav_text_color = $('#menu_nav_text_color').val();
			menu_nav_text_color_hover = $('#menu_nav_text_color_hover').val();
			menu_nav_border_color = $('#menu_nav_border_color').val();
			top_header_text_color = $('#top_header_text_color').val();
			top_header_text_color_hover = $('#top_header_text_color_hover').val();
			top_header_bg_color = $('#top_header_bg_color').val();
			product_price_color = $('#product_price_color').val();
			product_price_color_hover = $('#product_price_color_hover').val();
			product_subtitle_color = $('#product_subtitle_color').val();
			product_heading_color = $('#product_heading_color').val();
			product_heading_bgcolor = $('#product_heading_bgcolor').val();
			product_subtitle_color_hover = $('#product_subtitle_color_hover').val();
			validation_fa_icon_paragraph = $('#validation_fa_icon_paragraph').val();
			fa_icon_bgcolor = $('#fa_icon_bgcolor').val();
			fa_icon_bgcolor_hover = $('#fa_icon_bgcolor_hover').val();
			fa_icon_text_color = $('#fa_icon_text_color').val();
			fa_icon_text_color_hover = $('#fa_icon_text_color_hover').val();
			paragraph_color = $('#paragraph_color').val();
			validation_color = $('#validation_color').val();
		});
					
	});
	
	
</script>

</html>
