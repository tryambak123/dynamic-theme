<?php 
$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=cms", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}


function getThemeData($theme_id){
	global $conn;
	$sql = "SELECT * FROM themes WHERE id = ?";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array($theme_id));
	$res = $stmt->fetch(PDO::FETCH_ASSOC);
	
	return $res;
}

function getThemeDataByName($theme_name){
	global $conn;
	$sql = "SELECT * FROM themes WHERE name = ?";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array($theme_name));
	$res = $stmt->fetch(PDO::FETCH_ASSOC);
	
	return $res;
}

function getThemeList(){
	global $conn;
	$sql = "SELECT id,name FROM themes WHERE status = 1";
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $res;
}

function saveThemeConfig(){
	global $conn;
	$data = $_POST;
	unset($data['dd-themes']);
	unset($data['submit']);

	$pairs = [];
	foreach($data as $key => $val){
		$pairs[] = $key . " = '" . $val . "'";	
	}

	$str_pairs = implode(',', $pairs);

	$sql = "UPDATE themes SET ". $str_pairs . " WHERE name='".$_POST['dd-themes']."'";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array());
	
	return $stmt->rowCount();
}

function getSectionCSS($theme, $section){
	
	$str = file_get_contents($theme.'/assets/css/style.css');
	
	$str_arr = explode('/*'.$section.'*/', $str);	
	
	//echo '<pre>';print_r($str_arr[1]);
	
	$css = $str_arr[1];
	
	$css_arr = (BreakCSS($css));
	$css_arr = array_chunk($css_arr, 2, true);
	return $css_arr;
}

function BreakCSS($css)
{
    $results = array();

    preg_match_all('/(.+?)\s?\{\s?(.+?)\s?\}/', $css, $matches);
	
    foreach($matches[0] AS $i=>$original)
        foreach(explode(';', $matches[2][$i]) AS $attr)
            if (strlen(trim($attr)) > 0) // for missing semicolon on last element, which is legal
            {
                list($name, $value) = explode(':', $attr);
                $results[str_replace(array('.',' '),array('=dot=','=sp='),$matches[1][$i])][trim($name)] = trim($value);
            }
			
	//echo '<pre>';print_r($results);die;
    return $results;
}

function saveSectionCSS($theme, $section, $css){
	//echo $theme . '===' . $section . '<br/>' . $css;die;
	$str = file_get_contents($theme.'/assets/css/style.css');
	$str_arr = explode($section, $str);	
	$str_arr[1] = $css.'}';
	
	//die($str_arr[1]);
	$section = "\n".$section."\n";
	$str = implode($section, $str_arr);
	file_put_contents($theme.'/assets/css/style.css',$str);
}

?>