<div class="col-lg-4 col">
	<div class="row">
		<div class="col-lg-12 col">
			<div class="portlet box portlet-grey">
				<div class="portlet-header">
					<div class="caption">Themes</div>
					<div class="tools"></div>
				</div>
				<div class="portlet-body portlet-body-row-1">
					<div class="row">
						<div class="col-md-6">
							<select name="dd-themes" id="dd-themes"><?php 
								foreach($themes as $key => $data){?>
									<option value="<?php echo $data['name'] ?>"><?php echo $data['name']?></option><?php
								}?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?php 
	$theme = 'default';
	$sections = array('notification_bar','header', 'store_features', 'footer_top', 'footer_bottom');
	foreach($sections as $key => $section){ ?>
		<div class="row">
			<div class="col-lg-12 col">
				<div class="portlet box portlet-grey">
					<div class="portlet-header">
						<div class="caption"><?= ucwords(str_replace('_',' ',$section)); ?></div>
					</div>
					<div class="portlet-body portlet-body-row-1">
						<form name="frm-<?= $section?>" id="frm-<?= $section?>" method="post" action="saveChanges.php"><?php	
							$css_arr = getSectionCSS($theme, $section);
							//echo '<pre>';print_r($css_arr);die;
							$counter = 1;
							foreach($css_arr as $key => $chunk){ ?>
								<div class="row"><?php
									foreach($chunk as $key => $data){?>
										<div class="col-md-6">
											<div class="form-group">
												<div class="accordion-kvs" id="accordion-kvs-<?php echo $counter?>">
													<h3 title="<?php echo $key?>"><?php echo substr(str_replace(array('=dot=','=sp='),array('.',' '),$key), 0, 20) ?></h3>
													<div style="padding: 0em 0.2em"><?php 
														foreach($data as $slnk => $val){ ?>
															<label><?php echo $slnk ?></label><?php
															if(($slnk == 'color') || ($slnk == 'background-color') || ($slnk == 'background')){?>
																<input type="color" name="<?php echo $slnk .'_p_'.$key?>" value="<?php echo $val ?>" class="form-control required" style="color:#130202"><?php
															} else {?>
																<input type="text" name="<?php echo $slnk .'_p_'.$key?>" value="<?php echo $val ?>" class="form-control required" style="color:#130202"><?php
															}	
														}?>
													</div>
												</div>											
											</div>
										</div><?php
										$counter++;	
									}?>
								</div><?php
							}?>
							<input type="hidden" name="section" value="<?php echo $section?>"/>
							<input type="hidden" name="theme" value="<?php echo $theme?>"/>
							<div class="row">
								<div class="col-md-6">
									<a href="javascript:void(0)" name="apply" value="apply" section="<?= $section ?>" class="btn btn-dark applyChanges">Apply</a>
									<button name="save" type="submit" value="save" class="btn btn-primary">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><?php
	}?>
</div>