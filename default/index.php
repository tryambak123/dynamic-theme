<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>Avone Multipurpose eCommerce Bootstrap 4 Html Template</title>
			<meta name="description" content="description">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link rel="shortcut icon" href="assets/images/favicon.png" />

			<link rel="stylesheet" href="assets/css/plugins.css">

			<link rel="stylesheet" href="assets/css/style.css">
			<link rel="stylesheet" href="assets/css/responsive.css">
	</head>
	<body class="template-index index-demo1">
		<div id="pre-loader">
		<img src="assets/images/loader.gif" alt="Loading..." />
		</div>
	<div class="page-wrapper">

		<div class="notification-bar mobilehide">
			<a href="#" class="notification-bar__message"><b>Mutipurpose E-Commerce</b> PURCHASE THIS INCREDIBLE Temaplate NOW!</a>
			<span class="close-announcement"><i class="anm anm-times-l" aria-hidden="true"></i></span>
		</div>


		<header class="header animated d-flex align-items-center header-1">
			<div class="container">
				<div class="row">
					<div class="col-4 col-sm-4 col-md-4 d-block d-lg-none mobile-icons">
						<button type="button" class="btn--link site-header__menu js-mobile-nav-toggle mobile-nav--open">
							<i class="icon anm anm-times-l"></i>
							<i class="anm anm-bars-r"></i>
						</button>
						<div class="site-search iconset">
							<i class="icon anm anm-search-l"></i>
						</div>
					</div>
					<div class="logo col-4 col-sm-4 col-md-4 col-lg-2 align-self-center">
						<a href="index.html">
						<img src="assets/images/avon-logo.svg" alt="Avone Multipurpose Html Template" title="Avone Multipurpose Html Template" />
						</a>
					</div>

					<div class="col-1 col-sm-1 col-md-1 col-lg-8 align-self-center d-menu-col">
						<nav class="grid__item" id="AccessibleNav">
							<ul id="siteNav" class="site-nav medium center hidearrow">
								<li class="lvl1 parent megamenu mdropdown"><a href="#;">Home <i class="anm anm-angle-down-l"></i></a>
									<div class="megamenu style1">
										<ul class="grid mmWrapper">
											<li class="grid__item large-up--one-whole">
												<ul class="grid">
													<li class="grid__item lvl-1 col-md-4 col-lg-4">
														<a href="#" class="site-nav lvl-1 menu-title">Homepages</a>
														<ul class="subLinks">
															<li class="lvl-2"><a href="index.html" class="site-nav lvl-2">Home 01 <span class="lbl nm_label3">Popular</span></a></li>
															<li class="lvl-2"><a href="index-demo2.html" class="site-nav lvl-2">Home 02 <span class="lbl nm_label3">Popular</span></a></li>
															<li class="lvl-2"><a href="index-demo3.html" class="site-nav lvl-2">Home 03</a></li>
															<li class="lvl-2"><a href="index-demo4.html" class="site-nav lvl-2">Home 04</a></li>
															<li class="lvl-2"><a href="index-demo5.html" class="site-nav lvl-2">Home 05</a></li>
															<li class="lvl-2"><a href="index-demo6.html" class="site-nav lvl-2">Home 06</a></li>
															<li class="lvl-2"><a href="index-demo7.html" class="site-nav lvl-2">Home 07</a></li>
															<li class="lvl-2"><a href="index-demo8.html" class="site-nav lvl-2">Home 08</a></li>
															<li class="lvl-2"><a href="index-demo9.html" class="site-nav lvl-2">Home 09</a></li>
														</ul>
													</li>
													<li class="grid__item lvl-1 col-md-4 col-lg-4">
														<a href="#" class="site-nav lvl-1 menu-title">Homepages</a>
														<ul class="subLinks">
															<li class="lvl-2"><a href="index-demo10.html" class="site-nav lvl-2">Home 10</a></li>
															<li class="lvl-2"><a href="index-demo11.html" class="site-nav lvl-2">Home 11</a></li>
															<li class="lvl-2"><a href="index-demo12.html" class="site-nav lvl-2">Home 12</a></li>
															<li class="lvl-2"><a href="index-demo13.html" class="site-nav lvl-2">Home 13</a></li>
															<li class="lvl-2"><a href="index-demo14.html" class="site-nav lvl-2">Home 14</a></li>
															<li class="lvl-2"><a href="index-demo15.html" class="site-nav lvl-2">Home 15</a></li>
															<li class="lvl-2"><a href="index-demo16.html" class="site-nav lvl-2">Home 16</a></li>
															<li class="lvl-2"><a href="index-demo17.html" class="site-nav lvl-2">Home 17</a></li>
															<li class="lvl-2"><a href="index-yoga-shop.html" class="site-nav lvl-2">Yoga Shop <span class="lbl nm_label2">New</span></a></li>
														</ul>
													</li>
													<li class="grid__item lvl-1 col-md-4 col-lg-4">
														<a href="#" class="site-nav lvl-1 menu-title">Homeskin</a>
														<ul class="subLinks">
															<li class="lvl-2"><a href="index-jewelry-store.html" class="site-nav lvl-2">Jewelry Store <span class="lbl nm_label2">New</span></a></li>
															<li class="lvl-2"><a href="index-pet-store.html" class="site-nav lvl-2">Pet Store <span class="lbl nm_label4">Hot</span></a></li>
															<li class="lvl-2"><a href="index-medical-demo.html" class="site-nav lvl-2">Medical</a></li>
															<li class="lvl-2"><a href="index-demo18.html" class="site-nav lvl-2">Furniture Store <span class="lbl nm_label4">Hot</span></a></li>
															<li class="lvl-2"><a href="index-christmas-1.html" class="site-nav lvl-2">Christmas 1 <span class="lbl nm_label2">New</span></a></li>
															<li class="lvl-2"><a href="index-christmas-2.html" class="site-nav lvl-2">Christmas 2 <span class="lbl nm_label2">New</span></a></li>
														</ul>
													</li>
												</ul> 
												<ul class="grid mm-Banners">
													<li class="grid__item large-up--one-half imageCol">
													<a href="#"><img class="lazyload" src="assets/images/megamenu-banner7.jpg" data-src="assets/images/megamenu-banner7.jpg" alt=""></a>
													</li>
													<li class="grid__item large-up--one-half imageCol">
													<a href="#"><img class="lazyload" src="assets/images/megamenu-banner8.jpg" data-src="assets/images/megamenu-banner8.jpg" alt=""></a>
													</li>
												</ul>
											</li>
										</ul>
									</div>
								</li>
								<li class="lvl1 parent megamenu"><a href="#">Shop <i class="anm anm-angle-down-l"></i></a>
									<div class="megamenu style4">
										<ul class="grid grid--uniform mmWrapper">
											<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Category Page</a>
												<ul class="subLinks">
													<li class="lvl-2"><a href="category-2columns.html" class="site-nav lvl-2">2 Columns with style1</a></li>
													<li class="lvl-2"><a href="category-3columns.html" class="site-nav lvl-2">3 Columns with style2</a></li>
													<li class="lvl-2"><a href="category-4columns.html" class="site-nav lvl-2">4 Columns with style3</a></li>
													<li class="lvl-2"><a href="category-5columns.html" class="site-nav lvl-2">5 Columns with style4</a></li>
													<li class="lvl-2"><a href="category-6columns.html" class="site-nav lvl-2">6 Columns with Fullwidth</a></li>
													<li class="lvl-2"><a href="category-7columns.html" class="site-nav lvl-2">7 Columns</a></li>
													<li class="lvl-2"><a href="category-empty.html" class="site-nav lvl-2">Category Empty</a></li>
												</ul>
											</li>
											<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Shop Page</a>
												<ul class="subLinks">
													<li class="lvl-2"><a href="shop-list-view.html" class="site-nav lvl-2">List View</a></li>
													<li class="lvl-2"><a href="shop-category-slideshow.html" class="site-nav lvl-2">Category Slideshow</a></li>
													<li class="lvl-2"><a href="shop-left-sidebar.html" class="site-nav lvl-2">Left Sidebar</a></li>
													<li class="lvl-2"><a href="shop-right-sidebar.html" class="site-nav lvl-2">Right Sidebar</a></li>
													<li class="lvl-2"><a href="shop-fullwidth.html" class="site-nav lvl-2">Fullwidth/No Sidebar</a></li>
													<li class="lvl-2"><a href="shop-no-sidebar.html" class="site-nav lvl-2">No Sidebar/No Filter</a></li>
													<li class="lvl-2"><a href="shop-category-slideshow.html" class="site-nav lvl-2">With category description</a></li>
												</ul>
											</li>
											<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Shop Page</a>
												<ul class="subLinks">
													<li class="lvl-2"><a href="shop-left-sidebar.html" class="site-nav lvl-2">Simple Heading</a></li>
													<li class="lvl-2"><a href="shop-small-heading.html" class="site-nav lvl-2">Small Heading</a></li>
													<li class="lvl-2"><a href="shop-no-sidebar.html" class="site-nav lvl-2">Big Heading With Image</a></li>
													<li class="lvl-2"><a href="shop-right-sidebar.html" class="site-nav lvl-2">Headings With Banner#1</a></li>
													<li class="lvl-2"><a href="shop-heading-with-banner2.html" class="site-nav lvl-2">Headings With Banner#2</a></li>
													<li class="lvl-2"><a href="swatches-style.html" class="site-nav lvl-2">Swatches Style</a></li>
													<li class="lvl-2"><a href="shop-right-sidebar.html" class="site-nav lvl-2">Classic Pagination</a></li>
												</ul>
											</li>
											<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Shop Other Page</a>
												<ul class="subLinks">
													<li class="lvl-2"><a href="my-wishlist.html" class="site-nav lvl-2">My Wishlist</a></li>
													<li class="lvl-2"><a href="cart-style1.html" class="site-nav lvl-2">Cart Page Style1</a></li>
													<li class="lvl-2"><a href="cart-style2.html" class="site-nav lvl-2">Cart Page Style2</a></li>
													<li class="lvl-2"><a href="checkout-style1.html" class="site-nav lvl-2">Checkout Page Style1</a></li>
													<li class="lvl-2"><a href="checkout-style2.html" class="site-nav lvl-2">Checkout Page Style2</a></li>
													<li class="lvl-2"><a href="compare-style1.html" class="site-nav lvl-2">Compare Page Style1</a></li>
													<li class="lvl-2"><a href="compare-style2.html" class="site-nav lvl-2">Compare Page Style2</a></li>
												</ul>
											</li>
										</ul>
										<div class="row clear">
											<div class="col-md-4 col-lg-4">
												<a href="#;"><img src="assets/images/megamenu-banner4.jpg" data-src="assets/images/megamenu-banner4.jpg" alt="" /></a>
											</div>
											<div class="col-md-4 col-lg-4">
												<a href="#;"><img src="assets/images/megamenu-banner4.jpg" data-src="assets/images/megamenu-banner4.jpg" alt="" /></a>
											</div>
											<div class="col-md-4 col-lg-4">
												<a href="#;"><img src="assets/images/megamenu-banner4.jpg" data-src="assets/images/megamenu-banner4.jpg" alt="" /></a>
											</div>
										</div>
									</div>
								</li>
								<li class="lvl1 parent megamenu"><a href="#">Features <i class="anm anm-angle-down-l"></i></a>
									<div class="megamenu style2">
										<ul class="grid mmWrapper">
											<li class="grid__item one-whole">
												<ul class="grid">
													<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Product Page</a>
														<ul class="subLinks">
															<li class="lvl-2"><a href="product-layout1.html" class="site-nav lvl-2">Product Layout1</a></li>
															<li class="lvl-2"><a href="product-layout2.html" class="site-nav lvl-2">Product Layout2</a></li>
															<li class="lvl-2"><a href="product-layout3.html" class="site-nav lvl-2">Product Layout3</a></li>
															<li class="lvl-2"><a href="product-layout4.html" class="site-nav lvl-2">Product Layout4</a></li>
															<li class="lvl-2"><a href="product-layout5.html" class="site-nav lvl-2">Product Layout5</a></li>
															<li class="lvl-2"><a href="product-layout6.html" class="site-nav lvl-2">Product Layout6</a></li>
															<li class="lvl-2"><a href="product-layout7.html" class="site-nav lvl-2">Product Layout7</a></li>
															<li class="lvl-2"><a href="product-accordian.html" class="site-nav lvl-2">Product Accordian</a></li>
															<li class="lvl-2"><a href="product-tabs-left.html" class="site-nav lvl-2">Product Tabs Left</a></li>
															<li class="lvl-2"><a href="product-tabs-center.html" class="site-nav lvl-2">Product Tabs Center</a></li>
														</ul>
													</li>
													<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Product Page Types</a>
														<ul class="subLinks">
															<li class="lvl-2"><a href="product-standard.html" class="site-nav lvl-2">Standard Product</a></li>
															<li class="lvl-2"><a href="product-variable.html" class="site-nav lvl-2">Variable Product</a></li>
															<li class="lvl-2"><a href="product-grouped.html" class="site-nav lvl-2">Grouped Product</a></li>
															<li class="lvl-2"><a href="product-layout1.html" class="site-nav lvl-2">New Product</a></li>
															<li class="lvl-2"><a href="product-layout2.html" class="site-nav lvl-2">Sale Product</a></li>
															<li class="lvl-2"><a href="product-outofstock.html" class="site-nav lvl-2">Out Of Stock Product</a></li>
															<li class="lvl-2"><a href="product-external-affiliate.html" class="site-nav lvl-2">External / Affiliate Product</a></li>
															<li class="lvl-2"><a href="product-layout1.html" class="site-nav lvl-2">Variable Image</a></li>
															<li class="lvl-2"><a href="product-layout4.html" class="site-nav lvl-2">Variable Select</a></li>
															<li class="lvl-2"><a href="prodcut-360-degree-view.html" class="site-nav lvl-2">360 Degree view</a></li>
														</ul>
													</li>
													<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1 menu-title">Top Brands</a>
														<div class="menu-brand-logo">
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
														</div>
														<div class="menu-brand-logo">
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
															<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" /></a>
														</div>
													</li>
													<li class="grid__item lvl-1 col-md-3 col-lg-3 p-0">
														<a href="#"><img src="assets/images/megamenu-banner3.jpg" alt="" /></a>
													</li>
												</ul>
											</li>
										</ul>
									</div>
								</li>
								<li class="lvl1 parent dropdown"><a href="#">Lookbook <i class="anm anm-angle-down-l"></i></a>
									<ul class="dropdown">
										<li><a href="lookbook-2columns.html" class="site-nav">2 Columns</a></li>
										<li><a href="lookbook-3columns.html" class="site-nav">3 Columns</a></li>
										<li><a href="lookbook-4columns.html" class="site-nav">4 Columns</a></li>
										<li><a href="lookbook-5columns.html" class="site-nav">5 Columns + Fullwidth</a></li>
										<li><a href="lookbook-shop.html" class="site-nav">Lookbook Shop</a></li>
									</ul>
								</li>
							<li class="lvl1 parent dropdown"><a href="#">Pages <i class="anm anm-angle-down-l"></i></a>
								<ul class="dropdown">
									<li><a href="brands-page.html" class="site-nav">Brands Page</a></li>
									<li><a href="login.html" class="site-nav">Login</a></li>
									<li><a href="my-account.html" class="site-nav">My Account</a></li>
									<li><a href="aboutus-style1.html" class="site-nav">About Us <i class="anm anm-angle-right-l"></i></a>
									<ul class="dropdown">
									<li><a href="aboutus-style1.html" class="site-nav">About Us Style1</a></li>
									<li><a href="aboutus-style2.html" class="site-nav">About Us Style2</a></li>
									</ul>
									</li>
									<li><a href="contact-style1.html" class="site-nav">Contact Us <i class="anm anm-angle-right-l"></i></a>
									<ul class="dropdown">
									<li><a href="contact-style1.html" class="site-nav">Contact Us Style1</a></li>
									<li><a href="contact-style2.html" class="site-nav">Contact Us Style2</a></li>
									</ul>
									</li>
									<li><a href="faqs-style1.html" class="site-nav">FAQs <i class="anm anm-angle-right-l"></i></a>
									<ul class="dropdown">
									<li><a href="faqs-style1.html" class="site-nav">FAQs Style1</a></li>
									<li><a href="faqs-style2.html" class="site-nav">FAQs Style2</a></li>
									</ul>
									</li>
									<li><a href="cms.html" class="site-nav">CMS</a></li>
									<li><a href="error-404.html" class="site-nav">Error 404</a></li>
									<li><a href="coming-soon.html" class="site-nav">Coming soon <span class="lbl nm_label1">New</span> </a></li>
								</ul>
							</li>
							<li class="lvl1 parent dropdown"><a href="#">Blog <i class="anm anm-angle-down-l"></i></a>
								<ul class="dropdown">
									<li><a href="blog-left-sidebar.html" class="site-nav">Left Sidebar</a></li>
									<li><a href="blog-right-sidebar.html" class="site-nav">Right Sidebar</a></li>
									<li><a href="blog-fullwidth.html" class="site-nav">Fullwidth</a></li>
									<li><a href="blog-2columns.html" class="site-nav">2 Columns</a></li>
									<li><a href="blog-3columns.html" class="site-nav">3 Columns</a></li>
									<li><a href="blog-4columns.html" class="site-nav">4 Columns</a></li>
									<li><a href="blog-single-post.html" class="site-nav">Single Post</a></li>
								</ul>
							</li>
							</ul>
						</nav>
					</div>
					<div class="col-4 col-sm-4 col-md-4 col-lg-2 align-self-center icons-col text-right">
						<div class="site-search iconset">
							<i class="icon anm anm-search-l"></i>
						</div>
						<div class="search-drawer">
							<div class="container">
								<span class="closeSearch anm anm-times-l"></span>
								<h3 class="title">What are you looking for?</h3>
								<div class="block block-search">
									<div class="block block-content">
										<form class="form minisearch" id="header-search" action="#" method="get">
											<label for="search" class="label"><span>Search</span></label>
											<div class="control">
												<div class="searchField">
													<div class="search-category">
														<select id="rgsearch-category">
														<option value="0">All Categories</option>
														<option value="4">Shop</option>
														<option value="6">- All</option>
														<option value="8">- Men</option>
														<option value="10">- Women</option>
														<option value="12">- Shoes</option>
														<option value="14">- Blouses</option>
														<option value="16">- Pullovers</option>
														<option value="18">- Bags</option>
														<option value="20">- Accessories</option>
														</select>
													</div>
													<div class="input-box">
														<input id="search" type="text" name="q" value="" placeholder="Search for products, brands..." class="input-text">
														<button type="submit" title="Search" class="action search" disabled=""><i class="icon anm anm-search-l"></i></button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>


						<div class="setting-link iconset">
							<i class="icon icon-settings"></i>
						</div>
						<div id="settingsBox">
							<div class="customer-links">
								<p><a href="login.html" class="btn">Login</a></p>
								<p class="text-center">New User? <a href="register.html" class="register">Create an Account</a></p>
								<p class="text-center">Default welcome msg!</p>
							</div>
							<div class="currency-picker">
								<span class="ttl">Select Currency</span>
								<ul id="currencies" class="cnrLangList">
									<li class="selected"><a href="#;">INR</a></li>
									<li><a href="#;">GBP</a></li>
									<li><a href="#;">CAD</a></li>
									<li><a href="#;">USD</a></li>
									<li><a href="#;">AUD</a></li>
									<li><a href="#;">EUR</a></li>
									<li><a href="#;">JPY</a></li>
								</ul>
							</div>
							<div class="language-picker">
								<span class="ttl">SELECT LANGUAGE</span>
								<ul id="language" class="cnrLangList">
									<li><a href="#">English</a></li><li><a href="#">French</a></li><li><a>German</a></li>
								</ul>
							</div>
						</div>


						<div class="wishlist-link iconset">
							<i class="icon anm anm-heart-l"></i>
							<span class="wishlist-count">0</span>
						</div>


						<div class="header-cart iconset">
							<a href="#" class="site-header__cart btn-minicart" data-toggle="modal" data-target="#minicart-drawer">
								<i class="icon anm anm-bag-l"></i>
								<span class="site-cart-count">2</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>


		<div class="mobile-nav-wrapper" role="navigation">
			<div class="closemobileMenu"><i class="icon anm anm-times-l pull-right"></i> Close Menu</div>
			<ul id="MobileNav" class="mobile-nav">
				<li class="lvl1 parent megamenu"><a href="index.html">Home <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="#" class="site-nav">Homepages<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="index.html" class="site-nav">Home 01 <span class="lbl nm_label3">Popular</span></a></li>
								<li><a href="index-demo2.html" class="site-nav">Home 02 <span class="lbl nm_label3">Popular</span></a></li>
								<li><a href="index-demo3.html" class="site-nav">Home 03</a></li>
								<li><a href="index-demo4.html" class="site-nav">Home 04</a></li>
								<li><a href="index-demo5.html" class="site-nav">Home 05</a></li>
								<li><a href="index-demo6.html" class="site-nav">Home 06</a></li>
								<li><a href="index-demo7.html" class="site-nav">Home 07</a></li>
								<li><a href="index-demo8.html" class="site-nav">Home 08</a></li>
								<li><a href="index-demo9.html" class="site-nav">Home 09</a></li>
							</ul>
						</li>
						<li><a href="#" class="site-nav">Homepages<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="index-demo10.html" class="site-nav">Home 10</a></li>
								<li><a href="index-demo11.html" class="site-nav">Home 11</a></li>
								<li><a href="index-demo12.html" class="site-nav">Home 12</a></li>
								<li><a href="index-demo13.html" class="site-nav">Home 13</a></li>
								<li><a href="index-demo14.html" class="site-nav">Home 14</a></li>
								<li><a href="index-demo15.html" class="site-nav">Home 15</a></li>
								<li><a href="index-demo16.html" class="site-nav">Home 16</a></li>
								<li><a href="index-demo17.html" class="site-nav">Home 17</a></li>
								<li><a href="index-yoga-shop.html" class="site-nav">Yoga Shop <span class="lbl nm_label2">New</span></a></li>
							</ul>
						</li>
						<li><a href="#" class="site-nav">Homeskin<i class="anm anm-plus-l"></i></a>
							<ul>
								<li class="lvl-2"><a href="index-jewelry-store.html" class="site-nav">Jewelry Store <span class="lbl nm_label2">New</span></a></li>
								<li class="lvl-2"><a href="index-pet-store.html" class="site-nav">Pet Store <span class="lbl nm_label4">Hot</span></a></li>
								<li class="lvl-2"><a href="index-medical-demo.html" class="site-nav">Medical</a></li>
								<li class="lvl-2"><a href="index-demo18.html" class="site-nav">Furniture Store <span class="lbl nm_label4">Hot</span></a></li>
								<li class="lvl-2"><a href="index-christmas-1.html" class="site-nav">Christmas 1 <span class="lbl nm_label2">New</span></a></li>
								<li class="lvl-2"><a href="index-christmas-2.html" class="site-nav">Christmas 2 <span class="lbl nm_label2">New</span></a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="lvl1 parent megamenu"><a href="#">Shop <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="#" class="site-nav">Category Page<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="category-2columns.html" class="site-nav">2 Columns with style1</a></li>
								<li><a href="category-3columns.html" class="site-nav">3 Columns with style2</a></li>
								<li><a href="category-4columns.html" class="site-nav">4 Columns with style3</a></li>
								<li><a href="category-5columns.html" class="site-nav">5 Columns with style4</a></li>
								<li><a href="category-6columns.html" class="site-nav">6 Columns with Fullwidth</a></li>
								<li><a href="category-7columns.html" class="site-nav">7 Columns</a></li>
								<li><a href="category-empty.html" class="site-nav last">Category Empty</a></li>
							</ul>
						</li>
						<li><a href="#" class="site-nav">Shop Page<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="shop-list-view.html" class="site-nav">List View</a></li>
								<li><a href="shop-category-slideshow.html" class="site-nav">Category Slideshow</a></li>
								<li><a href="shop-left-sidebar.html" class="site-nav">Left Sidebar</a></li>
								<li><a href="shop-right-sidebar.html" class="site-nav">Right Sidebar</a></li>
								<li><a href="shop-fullwidth.html" class="site-nav">Fullwidth/No Sidebar</a></li>
								<li><a href="shop-no-sidebar.html" class="site-nav">No Sidebar/No Filter</a></li>
								<li><a href="shop-category-slideshow.html" class="site-nav last">With category description</a></li>
							</ul>
						</li>
						<li><a href="#" class="site-nav">Shop Page<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="shop-left-sidebar.html" class="site-nav">Simple Heading</a></li>
								<li><a href="shop-small-heading.html" class="site-nav">Small Heading</a></li>
								<li><a href="shop-no-sidebar.html" class="site-nav">Big Heading With Image</a></li>
								<li><a href="shop-right-sidebar.html" class="site-nav">Headings With Banner#1</a></li>
								<li><a href="shop-heading-with-banner2.html" class="site-nav2">Headings With Banner#2</a></li>
								<li><a href="swatches-style.html" class="site-nav">Swatches Style</a></li>
								<li><a href="classic-pagination.html" class="site-nav last">Classic Pagination</a></li>
							</ul>
						</li>
						<li><a href="#" class="site-nav">Shop Other Page<i class="anm anm-plus-l"></i></a>
						<ul>
							<li><a href="my-wishlist.html" class="site-nav">My Wishlist</a></li>
							<li><a href="cart-style1.html" class="site-nav">Cart Page Style1</a></li>
							<li><a href="cart-style2.html" class="site-nav">Cart Page Style2</a></li>
							<li><a href="checkout-style1.html" class="site-nav">Checkout Page Style1</a></li>
							<li><a href="checkout-style2.html" class="site-nav">Checkout Page Style2</a></li>
							<li><a href="compare-style1.html" class="site-nav">Compare Page Style1</a></li>
							<li><a href="compare-style2.html" class="site-nav last">Compare Page Style2</a></li>
						</ul>
						</li>
					</ul>
				</li>
				<li class="lvl1 parent megamenu"><a href="product-layout1.html">Product <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="product-layout1.html" class="site-nav">Product Page<i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="product-layout1.html" class="site-nav">Product Layout1</a></li>
								<li><a href="product-layout2.html" class="site-nav">Product Layout2</a></li>
								<li><a href="product-layout3.html" class="site-nav">Product Layout3</a></li>
								<li><a href="product-layout4.html" class="site-nav">Product Layout4</a></li>
								<li><a href="product-layout5.html" class="site-nav">Product Layout5</a></li>
								<li><a href="product-layout6.html" class="site-nav">Product Layout6</a></li>
								<li><a href="product-layout7.html" class="site-nav">Product Layout7</a></li>
								<li><a href="product-accordian.html" class="site-nav">Product Accordian</a></li>
								<li><a href="product-tabs-left.html" class="site-nav">Product Tabs Left</a></li>
								<li><a href="product-tabs-center.html" class="site-nav last">Product Tabs Center</a></li>
							</ul>
						</li>
						<li><a href="short-description.html" class="site-nav">Product Page Types <i class="anm anm-plus-l"></i></a>
							<ul>
								<li><a href="product-standard.html" class="site-nav">Standard Product</a></li>
								<li><a href="product-variable.html" class="site-nav">Variable Product</a></li>
								<li><a href="product-grouped.html" class="site-nav">Grouped Product</a></li>
								<li><a href="product-layout1.html" class="site-nav">New Product</a></li>
								<li><a href="product-layout2.html" class="site-nav">Sale Product</a></li>
								<li><a href="product-outofstock.html" class="site-nav">Out Of Stock Product</a></li>
								<li><a href="product-external-affiliate.html" class="site-nav">External / Affiliate Product</a></li>
								<li><a href="product-layout1.html" class="site-nav">Variable Image</a></li>
								<li><a href="product-layout4.html" class="site-nav">Variable Select</a></li>
								<li><a href="prodcut-360-degree-view.html" class="site-nav last">360 Degree view</a></li>
								</ul>
						</li>
					</ul>
				</li>
				<li class="lvl1 parent megamenu"><a href="#">Lookbook <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="lookbook-2columns.html" class="site-nav">2 Columns</a></li>
						<li><a href="lookbook-3columns.html" class="site-nav">3 Columns</a></li>
						<li><a href="lookbook-4columns.html" class="site-nav">4 Columns</a></li>
						<li><a href="lookbook-5columns.html" class="site-nav">5 Columns + Fullwidth</a></li>
						<li><a href="lookbook-shop.html" class="site-nav last">Lookbook Shop</a></li>
					</ul>
				</li>
				<li class="lvl1 parent megamenu"><a href="about-us.html">Pages <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="brands-page.html" class="site-nav">Brands Page</a></li>
						<li><a href="login.html" class="site-nav">Login</a></li>
						<li><a href="my-account.html" class="site-nav">My Account</a></li>
						<li><a href="aboutus-style1.html" class="site-nav">About Us <i class="anm anm-plus-l"></i></a>
							<ul class="dropdown">
								<li><a href="aboutus-style1.html" class="site-nav">About Us Style1</a></li>
								<li><a href="aboutus-style2.html" class="site-nav">About Us Style2</a></li>
							</ul>
						</li>
						<li><a href="contact-style1.html" class="site-nav">Contact Us <i class="anm anm-plus-l"></i></a>
							<ul class="dropdown">
								<li><a href="contact-style1.html" class="site-nav">Contact Us Style1</a></li>
								<li><a href="contact-style2.html" class="site-nav">Contact Us Style2</a></li>
							</ul>
						</li>
						<li><a href="faqs-style1.html" class="site-nav">FAQs <i class="anm anm-plus-l"></i></a>
							<ul class="dropdown">
								<li><a href="faqs-style1.html" class="site-nav">FAQs Style1</a></li>
								<li><a href="faqs-style2.html" class="site-nav">FAQs Style2</a></li>
							</ul>
						</li>
						<li><a href="cms.html" class="site-nav">CMS</a></li>
						<li><a href="error-404.html" class="site-nav">Error 404</a></li>
						<li><a href="coming-soon.html" class="site-nav">Coming soon</a></li>
					</ul>
				</li>
				<li class="lvl1 parent megamenu"><a href="blog-left-sidebar.html">Blog <i class="anm anm-plus-l"></i></a>
					<ul>
						<li><a href="blog-left-sidebar.html" class="site-nav">Left Sidebar</a></li>
						<li><a href="blog-right-sidebar.html" class="site-nav">Right Sidebar</a></li>
						<li><a href="blog-fullwidth.html" class="site-nav">Fullwidth</a></li>
						<li><a href="blog-2columns.html" class="site-nav">2 Columns</a></li>
						<li><a href="blog-3columns.html" class="site-nav">3 Columns</a></li>
						<li><a href="blog-4columns.html" class="site-nav">4 Columns</a></li>
						<li><a href="blog-single-post.html" class="site-nav">Single Post</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="page-content">
			<div class="slideshow slideshow-wrapper">
				<div class="home-slideshow">
					<div class="slide">
						<div class="blur-up lazyload">
							<img class="blur-up lazyload" data-src="assets/images/slideshow-banner/dome1-banner1.jpg" src="assets/images/slideshow-banner/dome1-banner1.jpg" alt=" Start Selling Online Successfully" title=" Start Selling Online Successfully" />
							<div class="slideshow__text-wrap slideshow__overlay left">
								<div class="slideshow__text-content">
									<div class="wrap-caption anim-tru left style1">
										<p class="mega-small-title">ARE YOU READY?</p>
										<h2 class="h1 mega-title slideshow__title">Start Selling Online Successfully</h2>
										<span class="mega-subtitle slideshow__subtitle">Creative, Flexible and High Performance Html Template!</span>
										<span class="btn">Shop now</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="blur-up lazyload">
							<img class="blur-up lazyload" data-src="assets/images/slideshow-banner/dome1-banner1.jpg" src="assets/images/slideshow-banner/dome1-banner1.jpg" alt="The Powerful Theme You can Trust" title="The Powerful Theme You can Trust" />
							<div class="slideshow__text-wrap slideshow__overlay right">
								<div class="slideshow__text-content">
									<div class="wrap-caption anim-tru style1">
										<h2 class="h1 mega-title slideshow__title">The Powerful Template You can Trust</h2>
										<span class="mega-subtitle slideshow__subtitle">High Performance Delivered</span>
										<span class="btn">Shop now</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="container">
				<div class="collection-banners style1">
					<div class="grid-masonary banner-grid">
						<div class="grid-sizer"></div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 banner-item">
								<div class="collection-grid-item">
									<div class="img">
										<img class="blur-up lazyload" data-src="assets/images/collection-banner/womens-top.jpg" src="assets/images/collection-banner/womens-top.jpg" alt="" title=" " />
									</div>
									<div class="details center">
										<div class="inner">
											<h3 class="title">Women Tops</h3>
											<p>From world's top designer</p>
											<a href="#;" class="btn">Discover Now</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 banner-item">
								<div class="collection-grid-item">
									<div class="img">
										<img class="blur-up lazyload" data-src="assets/images/collection-banner/men-clothing.jpg" src="assets/images/collection-banner/men-clothing.jpg" alt="" title=" " />
									</div>
									<div class="details center">
										<div class="inner">
											<h3 class="title">Men Shirts</h3>
											<p>Up to 70% off on selected item</p>
											<a href="#;" class="btn">Shop Now</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 banner-item">
								<div class="collection-grid-item">
									<div class="img">
										<img class="blur-up lazyload" data-src="assets/images/collection-banner/denim.jpg" src="assets/images/collection-banner/denim.jpg" alt="" title=" " />
									</div>
									<div class="details center">
										<div class="inner">
											<h3 class="title">Denim</h3>
											 <p>Find your perfect outfit</p>
											<a href="#;" class="btn">Shop Now</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 banner-item">
								<div class="collection-grid-item">
									<div class="img">
										<img class="blur-up lazyload" data-src="assets/images/collection-banner/accesories.jpg" src="assets/images/collection-banner/accesories.jpg" alt="" title=" " />
									</div>
									<div class="details center">
										<div class="inner">
											<h3 class="title">Accessories</h3>
											<p>add finishing touch to your outfit</p>
											<a href="#;" class="btn">Shop Now</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="section product-slider">
					<div class="section-header">
						<h2>New Arrivals</h2>
						<p>Shop our new arrivals from established brands</p>
						</div>
					<div class="productSlider grid-products">
						<div class="col-12 item">

						<div class="product-image">

						<a href="product-layout1.html" class="product-img">

						<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<div class="product-labels"><span class="lbl on-sale">Sale</span></div>

						</a>


						<div class="button-set style1">
						<ul>
						<li>

						<form class="add" action="cart-variant1.html" method="post">
						<button class="btn-icon btn btn-addto-cart" type="button" tabindex="0">
						<i class="icon anm anm-cart-l"></i>
						<span class="tooltip-label">Add to Cart</span>
						</button>
						</form>

						</li>
						<li>

						<a href="javascript:void(0)" title="Quick View" class="btn-icon quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
						<i class="icon anm anm-search-plus-l"></i>
						<span class="tooltip-label">Quick View</span>
						</a>

						</li>
						<li>

						<div class="wishlist-btn">
						<a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html">
						<i class="icon anm anm-heart-l"></i>
						<span class="tooltip-label">Add To Wishlist</span>
						</a>
						</div>

						</li>
						<li>

						<div class="compare-btn">
						<a class="btn-icon compare add-to-compare" href="compare-style2.html" title="Add to Compare">
						<i class="icon icon-reload"></i>
						<span class="tooltip-label">Add to Compare</span>
						</a>
						</div>

						</li>
						</ul>
						</div>

						</div>


						<div class="product-details text-center">

						<div class="product-name">
						<a href="product-layout1.html">Martha Knit Top</a>
						</div>


						<div class="product-price">
						<span class="price">$399.01</span>
						</div>


						<div class="product-review">
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star-o"></i>
						</div>


						<ul class="swatches">
						<li class="swatch small rounded navy"><span class="tooltip-label">Navy</span></li>
						<li class="swatch small rounded green"><span class="tooltip-label">Green</span></li>
						<li class="swatch small rounded gray"><span class="tooltip-label">Gray</span></li>
						<li class="swatch small rounded aqua"><span class="tooltip-label">Aqua</span></li>
						<li class="swatch small rounded orange"><span class="tooltip-label">Orange</span></li>
						</ul>

						</div>

						</div>
						<div class="col-12 item">

						<div class="product-image">

						<a href="product-layout1.html" class="product-img">

						<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">

						</a>


						<div class="saleTime desktop" data-countdown="2022/03/01"></div>


						<div class="button-set style1">
						<ul>
						<li>

						<form class="add" action="cart-variant1.html" method="post">
						<button class="btn-icon btn btn-addto-cart" type="button" tabindex="0">
						<i class="icon anm anm-cart-l"></i>
						<span class="tooltip-label">Add to Cart</span>
						</button>
						</form>

						</li>
						<li>

						<a href="javascript:void(0)" title="Quick View" class="btn-icon quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
						<i class="icon anm anm-search-plus-l"></i>
						<span class="tooltip-label">Quick View</span>
						</a>

						</li>
						<li>

						<div class="wishlist-btn">
						<a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html">
						<i class="icon anm anm-heart-l"></i>
						<span class="tooltip-label">Add To Wishlist</span>
						</a>
						</div>

						</li>
						<li>

						<div class="compare-btn">
						<a class="btn-icon compare add-to-compare" href="compare-style2.html" title="Add to Compare">
						<i class="icon icon-reload"></i>
						<span class="tooltip-label">Add to Compare</span>
						</a>
						</div>

						</li>
						 </ul>
						</div>

						</div>


						<div class="product-details text-center">

						<div class="product-name">
						<a href="product-layout1.html">Innerbloom Puffer Jacket</a>
						</div>


						<div class="product-price">
						<span class="price">$199.01</span>
						</div>


						<div class="product-review">
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						</div>


						<ul class="swatches">
						<li class="swatch small rounded black"><span class="tooltip-label">Black</span></li>
						<li class="swatch small rounded navy"><span class="tooltip-label">Navy</span></li>
						<li class="swatch small rounded purple"><span class="tooltip-label">Purple</span></li>
						</ul>

						</div>

						</div>
						<div class="col-12 item">

						<div class="product-image">

						<a href="product-layout1.html" class="product-img">

						<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">

						</a>


						<div class="product-labels"><span class="lbl pr-label1">New</span></div>


						<div class="button-set style1">
						<ul>
						<li>

						<form class="add" action="cart-variant1.html" method="post">
						<button class="btn-icon btn btn-addto-cart" type="button" tabindex="0">
						<i class="icon anm anm-cart-l"></i>
						<span class="tooltip-label">Add to Cart</span>
						</button>
						</form>

						</li>
						<li>

						<a href="javascript:void(0)" title="Quick View" class="btn-icon quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
						<i class="icon anm anm-search-plus-l"></i>
						<span class="tooltip-label">Quick View</span>
						</a>

						</li>
						<li>

						<div class="wishlist-btn">
						<a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html">
						<i class="icon anm anm-heart-l"></i>
						<span class="tooltip-label">Add To Wishlist</span>
						</a>
						</div>

						</li>
						<li>

						<div class="compare-btn">
						<a class="btn-icon compare add-to-compare" href="compare-style2.html" title="Add to Compare">
						<i class="icon icon-reload"></i>
						<span class="tooltip-label">Add to Compare</span>
						</a>
						</div>

						</li>
						</ul>
						</div>

						</div>


						<div class="product-details text-center">

							<div class="product-name">
							<a href="product-layout1.html">Button Up Top Black</a>
							</div>


							<div class="product-price">
							<span class="price">$99.01</span>
							</div>


							<div class="product-review">
							<i class="font-13 fa fa-star"></i>
							<i class="font-13 fa fa-star"></i>
							<i class="font-13 fa fa-star-o"></i>
							<i class="font-13 fa fa-star-o"></i>
							<i class="font-13 fa fa-star-o"></i>
							</div>


							<ul class="swatches">
								<li class="swatch small rounded red"><span class="tooltip-label">red</span></li>
								<li class="swatch small rounded orange"><span class="tooltip-label">orange</span></li>
								<li class="swatch small rounded yellow"><span class="tooltip-label">yellow</span></li>
							</ul>

						</div>

						</div>
						<div class="col-12 item">

						<div class="product-image">

						<a href="product-layout1.html" class="product-img">

						<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">

						<span class="sold-out"><span>Sold out</span></span>
						</a>

						</div>


						<div class="product-details text-center">

						<div class="product-name">
						<a href="product-layout1.html">Sunset Sleep Scarf Top</a>
						</div>


						<div class="product-price">
						<span class="price">$99.01</span>
						</div>


						<div class="product-review">
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						</div>


						<ul class="swatches">
						<li class="swatch small rounded black"><span class="tooltip-label">black</span></li>
						<li class="swatch small rounded navy"><span class="tooltip-label">navy</span></li>
						<li class="swatch small rounded darkgreen"><span class="tooltip-label">darkgreen</span></li>
						</ul>

						</div>

						</div>
						<div class="col-12 item">

						<div class="product-image">

						<a href="product-layout1.html" class="product-img">

						<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


						<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">

						</a>


						<div class="button-set style1">
						<ul>
						<li>

						<form class="add" action="cart-variant1.html" method="post">
						<button class="btn-icon btn btn-addto-cart" type="button" tabindex="0">
						<i class="icon anm anm-cart-l"></i>
						<span class="tooltip-label">Add to Cart</span>
						</button>
						</form>

						</li>
						<li>

						<a href="javascript:void(0)" title="Quick View" class="btn-icon quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
						<i class="icon anm anm-search-plus-l"></i>
						<span class="tooltip-label">Quick View</span>
						</a>

						</li>
						<li>

						<div class="wishlist-btn">
						<a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html">
						<i class="icon anm anm-heart-l"></i>
						<span class="tooltip-label">Add To Wishlist</span>
						</a>
						</div>

						</li>
						<li>

						<div class="compare-btn">
						<a class="btn-icon compare add-to-compare" href="compare-style2.html" title="Add to Compare">
						<i class="icon icon-reload"></i>
						<span class="tooltip-label">Add to Compare</span>
						</a>
						</div>

						</li>
						</ul>
						</div>

						</div>


						<div class="product-details text-center">

						<div class="product-name">
						<a href="product-layout1.html">Backpack With Contrast Bow</a>
						</div>


						<div class="product-price">
						<span class="price">$39.01</span>
						</div>


						<div class="product-review">
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star"></i>
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						<i class="font-13 fa fa-star-o"></i>
						</div>


						<ul class="swatches">
						<li class="swatch small rounded black"><span class="tooltip-label">black</span></li>
						<li class="swatch small rounded maroon"><span class="tooltip-label">maroon</span></li>
						</ul>

						</div>

						</div>
						<div class="col-12 item">
							<div class="product-image">

							<a href="product-layout1.html" class="product-img">

							<img class="primary blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">


							<img class="hover blur-up lazyload" data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">

							</a>


							<div class="product-labels"><span class="lbl pr-label2">Hot</span></div>
							<div class="button-set style1">
								<ul>
									<li>

									<form class="add" action="cart-variant1.html" method="post">
									<button class="btn-icon btn btn-addto-cart" type="button" tabindex="0">
									<i class="icon anm anm-cart-l"></i>
									<span class="tooltip-label">Add to Cart</span>
									</button>
									</form>

									</li>
									<li>

									<a href="javascript:void(0)" title="Quick View" class="btn-icon quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
									<i class="icon anm anm-search-plus-l"></i>
									<span class="tooltip-label">Quick View</span>
									</a>

									</li>
									<li>

									<div class="wishlist-btn">
									<a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html">
									<i class="icon anm anm-heart-l"></i>
									<span class="tooltip-label">Add To Wishlist</span>
									</a>
									</div>

									</li>
									<li>

									<div class="compare-btn">
									<a class="btn-icon compare add-to-compare" href="compare-style2.html" title="Add to Compare">
									<i class="icon icon-reload"></i>
									<span class="tooltip-label">Add to Compare</span>
									</a>
									</div>

									</li>
								</ul>
							</div>
							</div>
							<div class="product-details text-center">
								<div class="product-name">
									<a href="product-layout1.html">Toledo Mules shoes</a>
								</div>
								<div class="product-price">
									<span class="price">$299.01</span>
								</div>
								<div class="product-review">
									<i class="font-13 fa fa-star"></i>
									<i class="font-13 fa fa-star"></i>
									<i class="font-13 fa fa-star"></i>
									<i class="font-13 fa fa-star-o"></i>
									<i class="font-13 fa fa-star-o"></i>
								</div>
								<ul class="swatches">
									<li class="swatch small rounded gray"><span class="tooltip-label">gray</span></li>
									<li class="swatch small rounded red"><span class="tooltip-label">red</span></li>
									<li class="swatch small rounded orange"><span class="tooltip-label">orange</span></li>
									<li class="swatch small rounded yellow"><span class="tooltip-label">yellow</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>


				<div class="collection-banners style1 section no-pt-section">
				<div class="row">
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 banner-item">
				<div class="imgBanner-grid-item">
				<div class="img">
				<a href="collection-page.html" class="collection-grid-item__link">
				<img data-src="assets/images/collection-banner/demo1-banr1.jpg" src="assets/images/collection-banner/demo1-banr1.jpg" alt="Fashion" class="blur-up lazyload" />
				</a>
				</div>
				<div class="details center w-50">
				<div class="inner">
				<h3 class="title">FIND THE BEST COLLECTION AROUND THE WORLD</h3>
				<a href="#;" class="btn">Shop Now</a>
				</div>
				</div>
				</div>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 banner-item">
				<div class="imgBanner-grid-item">
				<div class="img">
				<a href="collection-page.html" class="collection-grid-item__link">
				<img class="blur-up lazyload" data-src="assets/images/collection-banner/demo1-banr2.jpg" src="assets/images/collection-banner/demo1-banr1.jpg" alt="Cosmetic" />
				</a>
				</div>
				<div class="details center w-50">
				<div class="inner">
				<h3 class="title">AWESOME T-SHIRTS, CROP TOPS AND MORE...</h3>
				<a href="#;" class="btn">Shop Now</a>
				</div>
				</div>
				</div>
				</div>
				</div>
				</div>
			</div>


			<div class="section logo-section no-pt-section">
			<div class="container">
			<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
			<div class="logo-bar">
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			<div class="logo-bar__item">
			<a href="#;"><img src="assets/images/logo/brandlogo1.png" alt="" title="" /></a>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>

		</div>

		<div class="section home-blog-post">
			<div class="container">
				<div class="section-header">
					<h2>Fresh From Our Blog</h2>
					<p>You are going to love this amazing shopify theme.</p>
				</div>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<div class="blog-post-slider">
							<div class="blogpost-item">
								<a href="#;" class="post-thumb">
								<img src="assets/images/blog/post-img1.jpg" alt="" title="" />
								</a>
								<div class="post-detail">
									<h3 class="post-title"><a href="#;">Our development is your success</a></h3>
									<ul class="publish-detail">
									<li><span class="article__date">March 06, 2019</span></li>
									<li><i class="anm anm-comments-l"></i> <a href="#;" class="btn-link">1 comment</a></li>
									</ul>
								</div>
							</div>
							<div class="blogpost-item">
								<a href="#;" class="post-thumb">
								<img src="assets/images/blog/post-img1.jpg" alt="" title="" />
								</a>
								<div class="post-detail">
									<h3 class="post-title"><a href="#;">Ensuring Customer Success</a></h3>
									<ul class="publish-detail">
									<li><span class="article__date">February 11, 2019</span></li>
									</ul>
								</div>
							</div>
							<div class="blogpost-item">
								<a href="#;" class="post-thumb">
								<img src="assets/images/blog/post-img1.jpg" alt="" title="" />
								</a>
								<div class="post-detail">
									<h3 class="post-title"><a href="#;">We can make your business shine!</a></h3>
									<ul class="publish-detail">
										<li><span class="article__date">February 19, 2019</span></li>
										<li><i class="anm anm-comments-l"></i> <a href="#;" class="btn-link">2 comments</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="store-features">
			<div class="container">
				<div class="row store-info">
					<div class="col-12 col-sm-6 col-md-4 col-lg-4">
						<i class="anm anm-free-delivery" aria-hidden="true"></i>
						<h5>Free Shipping &amp; Return</h5>
						<p class="sub-text">Free shipping on all US orders</p>
					</div>
					<div class="col-12 col-sm-6 col-md-4 col-lg-4">
						<i class="anm anm-money" aria-hidden="true"></i>
						<h5>Money Guarantee</h5>
						<p class="sub-text">30 days money back guarantee</p>
					</div>
					<div class="col-12 col-sm-6 col-md-4 col-lg-4">
						<i class="anm anm-phone-24" aria-hidden="true"></i>
						<h5>Online Support</h5>
						<p class="sub-text">We support online 24/7 on day</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="footer footer-1">
		<div class="footer-top clearfix">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-4 col-lg-3 about-us-col">
						<img src="assets/images/footer-logo.png" alt="Avone" />
						<p>55 Gallaxy Enque,<br>2568 steet, 23568 NY</p>
						<p><b>Phone</b>: (440) 000 000 0000</p>
						<p><b>Email</b>: <a href="/cdn-cgi/l/email-protection#1e6d7f727b6d5e67716b6d776a7b307d7173"><span class="__cf_email__" data-cfemail="bdcedcd1d8cefdc4d2c8ced4c9d893ded2d0">[email&#160;protected]</span></a></p>
						<ul class="list--inline social-icons">
							<li><a href="#" target="_blank"><i class="icon icon-facebook"></i></a></li>
							<li><a href="#" target="_blank"><i class="icon icon-twitter"></i></a></li>
							<li><a href="#" target="_blank"><i class="icon icon-pinterest"></i></a></li>
							<li><a href="#" target="_blank"><i class="icon icon-instagram"></i></a></li>
							<li><a href="#" target="_blank"><i class="icon icon-youtube"></i></a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-3 footer-links">
						<h4 class="h4">Quick Shop</h4>
						<ul>
							<li><a href="#">Women</a></li>
							<li><a href="#">Men</a></li>
							<li><a href="#">Kids</a></li>
							<li><a href="#">Sportswear</a></li>
							<li><a href="#">Sale</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-3 footer-links">
						<h4 class="h4">Informations</h4>
						<ul>
							<li><a href="aboutus-style1.html">About us</a></li>
							<li><a href="login.html">Login</a></li>
							<li><a href="#">Privacy policy</a></li>
							<li><a href="#">Terms &amp; condition</a></li>
							<li><a href="my-account.html">My Account</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-3 footer-links">
						<h4 class="h4">Customer Services</h4>
						<ul>
							<li><a href="#">Request Personal Data</a></li>
							<li><a href="faqs-style1.html">FAQ's</a></li>
							<li><a href="contact-style1.html">Contact Us</a></li>
							<li><a href="#">Orders and Returns</a></li>
							<li><a href="#">Support Center</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-3 newsletter-col">
						<div class="display-table">
							<div class="display-table-cell footer-newsletter">
								<form action="#" method="post">
									<label class="h4">Newsletter</label>
									<p>Enter your email to receive daily news and get 20% off coupon for all items.</p>
									<div class="input-group">
										<input type="email" class="input-group__field newsletter-input" name="EMAIL" value="" placeholder="Email address" required>
										<span class="input-group__btn">
										<button type="submit" class="btn newsletter__submit" name="commit" id="Subscribe"><span class="newsletter__submit-text--large">Subscribe</span></button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom clearfix">
			<div class="container">
				<ul class="payment-icons list--inline">
					<li><i class="anm anm-cc-visa" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-mastercard" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-amex" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-paypal" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-discover" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-stripe" aria-hidden="true"></i></li>
					<li><i class="anm anm-cc-jcb" aria-hidden="true"></i></li>
				</ul>
				<div class="copytext">&copy; 2020 Avone. All Rights Reserved.</div>
			</div>
		</div>
	</div>


	<span id="site-scroll"><i class="icon anm anm-arw-up"></i></span>


	<div class="minicart-right-drawer modal right fade" id="minicart-drawer">
		<div class="modal-dialog">
			<div class="modal-content">
				<div id="cart-drawer" class="block block-cart">
					<a href="javascript:void(0);" class="close-cart" data-dismiss="modal" aria-label="Close"><i class="anm anm-times-r"></i></a>
					<h4>Your cart (2 Items)</h4>
					<div class="minicart-content">
						<ul class="clearfix">
							<li class="item clearfix">
								<a class="product-image" href="#">
								<img src="assets/images/product-images/cart-page-img1.jpg" alt="" title="">
								</a>
								<div class="product-details">
									<a href="#" class="remove"><i class="anm anm-times-sql" aria-hidden="true"></i></a>
									<a href="#" class="edit-i remove"><i class="icon icon-pencil" aria-hidden="true"></i></a>
									<a class="product-title" href="cart-style1.html">Backpack With Contrast Bow</a>
									<div class="variant-cart">Black / XL</div>
									<div class="wrapQtyBtn">
										<div class="qtyField">
											<a class="qtyBtn minus" href="javascript:void(0);"><i class="anm anm-minus-r" aria-hidden="true"></i></a>
											<input type="text" name="quantity" value="1" class="qty">
											<a class="qtyBtn plus" href="javascript:void(0);"><i class="anm anm-plus-r" aria-hidden="true"></i></a>
										</div>
									</div>
									<div class="priceRow">
										<div class="product-price">
											<span class="money">$59.00</span>
										</div>
									</div>
								</div>
							</li>
							<li class="item clearfix">
								<a class="product-image" href="#">
									<img src="assets/images/product-images/cart-page-img1.jpg" alt="" title="">
								</a>
								<div class="product-details">
									<a href="#" class="remove"><i class="anm anm-times-sql" aria-hidden="true"></i></a>
									<a href="#" class="edit-i remove"><i class="icon icon-pencil" aria-hidden="true"></i></a>
									<a class="product-title" href="cart-style1.html">Innerbloom Puffer</a>
									<div class="variant-cart">Red / S</div>
									<div class="wrapQtyBtn">
										<div class="qtyField">
											<a class="qtyBtn minus" href="javascript:void(0);"><i class="anm anm-minus-r" aria-hidden="true"></i></a>
											<input type="text" name="quantity" value="1" class="qty">
											<a class="qtyBtn plus" href="javascript:void(0);"><i class="anm anm-plus-r" aria-hidden="true"></i></a>
										</div>
									</div>
									<div class="priceRow">
										<div class="product-price">
											<span class="money">$89.00</span>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="minicart-bottom">
						<div class="subtotal list">
							<span>Shipping:</span>
							<span class="product-price">$10.00</span>
						</div>
						<div class="subtotal list">
							<span>Tax:</span>
							<span class="product-price">$05.00</span>
						</div>
						<div class="subtotal">
							<span>Total:</span>
							<span class="product-price">$93.13</span>
						</div>
						<button type="button" class="btn proceed-to-checkout">Proceed to Checkout</button>
						<button type="button" class="btn btn-secondary cart-btn">View Cart</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="loadingBox">
		<div class="anm-spin"><i class="anm anm-spinner4"></i></div>
	</div>
	<div class="modalOverly"></div>
	<div id="quickView-modal" class="mfp-with-anim mfp-hide">
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<div id="slider">
					<div id="myCarousel" class="carousel slide">
						<div class="carousel-inner">
							<div class="item carousel-item active" data-slide-number="0">
								<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
							</div>
							<div class="item carousel-item" data-slide-number="1">
								<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
							</div>
							<div class="item carousel-item" data-slide-number="2">
								<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
							</div>
							<div class="item carousel-item" data-slide-number="3">
								<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
							</div>
							<div class="item carousel-item" data-slide-number="4">
								<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
							</div>
						</div>

						<div class="model-thumbnail-img">
							<ul class="carousel-indicators list-inline">
								<li class="list-inline-item active">
									<a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#myCarousel">
									<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
									</a>
								</li>
								<li class="list-inline-item">
									<a id="carousel-selector-1" data-slide-to="1" data-target="#myCarousel">
									<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
									</a>
								</li>
								<li class="list-inline-item">
									<a id="carousel-selector-2" class="selected" dataslide-to="2" data-target="#myCarousel">
									<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
									</a>
								</li>
								<li class="list-inline-item">
									<a id="carousel-selector-3" data-slide-to="3" data-target="#myCarousel">
									<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
									</a>
								</li>
								<li class="list-inline-item">
									<a id="carousel-selector-4" data-slide-to="4" data-target="#myCarousel">
									<img data-src="assets/images/product-images/product1.jpg" src="assets/images/product-images/product1.jpg" alt="" title="">
									</a>
								</li>
							</ul>
							<a class="carousel-control left" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
							<a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6 col-lg-6">
				<div class="product-brand"><a href="#">Charcoal</a></div>
				<h2 class="product-title">Product Quick View Popup</h2>
				<div class="product-review">
					<div class="rating">
						<i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i>
					</div>
					<div class="reviews"><a href="#">5 Reviews</a></div>
				</div>
				<div class="product-info">
					<div class="product-stock"> <span class="instock">In Stock</span> <span class="outstock hide">Unavailable</span> </div>
					<div class="product-sku">SKU: <span class="variant-sku">19115-rdxs</span></div>
				</div>
				<div class="pricebox">
					<span class="price old-price">$900.00</span>
					<span class="price">$800.00</span>
				</div>
				<div class="sort-description">Avone Multipurpose Bootstrap 4 Html Template that will give you and your customers a smooth shopping experience which can be used for various kinds of stores such as fashion.. </div>
				<form method="post" action="#" id="product_form--option" class="product-form">
					<div class="product-options">
						<div class="swatch clearfix swatch-0 option1">
							<div class="product-form__item">
								<label class="label">Color:<span class="required">*</span> <span class="slVariant">Red</span></label>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-black" type="radio" name="option-0" value="Black">
									<label class="swatchLbl small black" for="swatch-black" title="Black"></label>
								</div>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-blue" type="radio" name="option-0" value="blue">
									<label class="swatchLbl small blue" for="swatch-blue" title="Blue"></label>
								</div>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-red" type="radio" name="option-0" value="Blue">
									<label class="swatchLbl small red" for="swatch-red" title="Red"></label>
								</div>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-pink" type="radio" name="option-0" value="Pink">
									<label class="swatchLbl color small pink" for="swatch-pink" title="Pink"></label>
								</div>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-orange" type="radio" name="option-0" value="Orange">
									<label class="swatchLbl color small orange" for="swatch-orange" title="Orange"></label>
								</div>
								<div class="swatch-element color">
									<input class="swatchInput" id="swatch-yellow" type="radio" name="option-0" value="Yellow">
									<label class="swatchLbl color small yellow" for="swatch-yellow" title="Yellow"></label>
								</div>
							</div>
						</div>
						<div class="swatch clearfix swatch-1 option2">
							<div class="product-form__item">
								<label class="label">Size:<span class="required">*</span> <span class="slVariant">XS</span></label>
								<div class="swatch-element xs">
									<input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
									<label class="swatchLbl medium" for="swatch-1-xs" title="XS">XS</label>
								</div>
								<div class="swatch-element s">
									<input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
									<label class="swatchLbl medium" for="swatch-1-s" title="S">S</label>
								</div>
								<div class="swatch-element m">
									<input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
									<label class="swatchLbl medium" for="swatch-1-m" title="M">M</label>
								</div>
								<div class="swatch-element l">
									<input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="L">
									<label class="swatchLbl medium" for="swatch-1-l" title="L">L</label>
								</div>
							</div>
						</div>
						<div class="product-action clearfix">
							<div class="quantity">
								<div class="wrapQtyBtn">
									<div class="qtyField">
										<a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
										<input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
										<a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
										</div>
								</div>
							</div>
							<div class="add-to-cart">
								<button type="button" class="btn button-cart">
								<span>Add to cart</span>
								</button>
							</div>
						</div>
					</div>
				</form>
				<div class="wishlist-btn">
					<a class="wishlist add-to-wishlist" href="#" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Add to Wishlist</span></a>
				</div>
				<div class="share-icon">
					<span>Share:</span>
					<ul class="list--inline social-icons">
						<li><a href="#" target="_blank"><i class="icon icon-facebook"></i></a></li>
						<li><a href="#" target="_blank"><i class="icon icon-twitter"></i></a></li>
						<li><a href="#" target="_blank"><i class="icon icon-pinterest"></i></a></li>
						<li><a href="#" target="_blank"><i class="icon icon-instagram"></i></a></li>
						<li><a href="#" target="_blank"><i class="icon icon-youtube"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<div id="newsletter-modal" class="style2 mfp-with-anim mfp-hide">
		<div class="newsltr-tbl">
			<div class="newsltr-img small--hide"><img src="assets/images/newsletter/newsletter_540.jpg" alt=""></div>
			<div class="newsltr-text text-center">
				<div class="wraptext">
					<h2>Join Our Mailing List</h2>
					<p class="sub-text">Stay Informed! Monthly Tips, Tracks and Discount. </p>
					<form action="#" class="mcNewsletter" method="post">
						<div class="input-group">
							<input type="email" class="newsletter__input" name="EMAIL" value="" placeholder="Email address" required>
							<span class="">
							<button type="submit" class="btn mcNsBtn" name="commit"><span>Subscribe</span></button>
							</span>
						</div>
					</form>
					<ul class="list--inline social-icons">
						<li><a class="si-link" href="#" title="Facebook" target="_blank"><i class="anm anm-facebook-f" aria-hidden="true"></i></a></li>
						<li><a class="si-link" href="#" title="Twitter" target="_blank"><i class="anm anm-twitter" aria-hidden="true"></i></a></li>
						<li><a class="si-link" href="#" title="Pinterest" target="_blank"><i class="anm anm-pinterest-p" aria-hidden="true"></i></a></li>
						<li><a class="si-link" href="#" title="Instagram" target="_blank"><i class="anm anm-instagram" aria-hidden="true"></i></a></li>
					</ul>
					<p class="checkboxlink">
						<input type="checkbox" id="dontshow">
						<label for="dontshow">Don't show this popup again</label>
					</p>
				</div>
			</div>
		</div>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	</div>


	<div class="product-notification" id="dismiss">
		<span class="close" aria-hidden="true"><i class="anm anm-times-r"></i></span>
		<div class="media">
			<img class="mr-2" src="assets/images/product-images/95x120.jpg" alt="Generic placeholder image" />
			<div class="media-body">
				<h5 class="mt-0 mb-1">Someone purchsed a</h5>
				<p class="pname">Lorem ipsum dolor sit amet</p>
				<p class="detail">14 Minutes ago from New York, USA</p>
			</div>
		</div>
	</div>


	<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
	<script src="assets/js/vendor/js.cookie.js"></script>

	<script src="assets/js/plugins.js"></script>
	<script src="assets/js/main.js"></script>

	<script>
			function newsletter_popup(){
				var cookieSignup="cookieSignup", date=new Date();
				if($.cookie(cookieSignup) !='true' && window.location.href.indexOf("challenge#newsletter-modal") <=-1) {
					setTimeout( function() {
						$.magnificPopup.open( {
							items: {
								src: '#newsletter-modal'
							}
							, type:'inline', removalDelay:300, mainClass: 'mfp-zoom-in'
						}
						);
					}
					, 5000);
				}
				$.magnificPopup.instance.close=function () {
					if($("#dontshow").prop("checked")==true) {
						$.cookie(cookieSignup, 'true', {
							expires: 1, path: '/'
						}
					);
				}
					$.magnificPopup.proto.close.call(this);
				}
			}
			newsletter_popup();
		</script>

	</body>
</html>