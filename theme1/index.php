<html>
	<?php 
		include('../functions.php');
		$theme_id = 1;
		$theme_data = getThemeData($theme_id); ?>
	<head>
		<title>Theme1</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<!-- Optional theme -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="css/custom.css">
		
	</head>
	<body>
	
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col header" id="header">
					<div class="col-lg-12 col">
						<div class="row" id="menu_nav">
							<div class="col-sm-2 col menu-opt">File</div>
							<div class="col-sm-2 col menu-opt">Edit</div>
							<div class="col-sm-2 col menu-opt">View</div>
							<div class="col-sm-2 col menu-opt"><input class="btn" type="button" id="btn-login" value="Login" style="background-color:<?php echo $theme_data['button_bg_color']?>"/></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 col">
					<div class="row">
						<div class="col-lg-12 col" style="background-color:red">
							<div class="row">
								<div class="col-lg-12 col" style="background-color:white">Option1</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:black">Option2</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:white">Option3</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:black">Option4</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:white">Option5</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:black">Option6</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:white">Option7</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col" style="background-color:black">Option8</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col product-container">
					<div class="row" style="height:100%;max-height:100%;">
						<div class="col-lg-12 col" style="background-color:green">Container</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col" style="background-color:<?php echo $theme_data['footer_bg_color']?>;color:<?php echo $theme_data['footer_text_color']?>">
					<div class="row footer-heading" style="background-color:<?php echo $theme_data['footer_heading_color']?>;">
						<div class="col-sm-12 col">Footer Heading</div>
					</div>
					<div class="row footer-mid" style="background-color:<?php echo $theme_data['footer_bg_color']?>;">
						<div class="col-sm-12 col">Footer Mid</div>
					</div>
					<div class="row footer-bottom" style="background-color:<?php echo $theme_data['footer_bottom_bg_color']?>;">
						<div class="col-sm-12 col">Footer Bottom</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>